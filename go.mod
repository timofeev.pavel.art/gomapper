module gitlab.com/timofeev.pavel.art/gomapper

go 1.19

require (
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	gitlab.com/timofeev.pavel.art/greet v0.0.0-20230208215341-9d705fcc7e01 // indirect
)
